package n26challenge;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static java.lang.Thread.sleep;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(properties = {"n26challenge.expireSeconds=60", "n26challenge.attendantPeriodMilliseconds=300"})
public class AttendantTest {

    @Autowired
    ApplicationContext applicationContext;

    @Mock
    Registrar registrar;

    @Autowired
    @InjectMocks
    Attendant attendant;

    @Before
    public void setUp() throws Exception {
        // Initialize mocks created above
        MockitoAnnotations.initMocks(this);
        // Change behaviour of `resource`
        //when(resource.getSomething()).thenReturn("Foo");
    }

    @Test
    public void testRegistrareInit() throws Exception {

        try {
            attendant.start();
            sleep(300);
            verify(registrar, atLeastOnce()).update();
        } finally {
            attendant.stop();
        }
    }
}
