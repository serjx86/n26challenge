package n26challenge;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.hamcrest.Matchers.isEmptyString;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(properties = {"n26challenge.expireSeconds=60", "n26challenge.attendantPeriodMilliseconds=30"})
public class ChallengeControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void postTransactionNow() throws Exception {
        String content = String.format("{\"amount\": 1.0, \"timestamp\": %s}", Instant.now().toEpochMilli());
        mvc.perform(MockMvcRequestBuilders.post("/transaction")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().string(isEmptyString()));
    }

    @Test
    public void postTransactionPastAMinute() throws Exception {
        String content = String.format("{\"amount\": 1.0, \"timestamp\": %s}",
                Instant.now().minus(61, ChronoUnit.SECONDS).toEpochMilli());
        mvc.perform(MockMvcRequestBuilders.post("/transaction")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent())
                .andExpect(content().string(isEmptyString()));
    }

    @Test
    public void getStatisticsOne() throws Exception {
        String content = String.format("{\"amount\": 1.0, \"timestamp\": %s}", Instant.now().toEpochMilli());
        mvc.perform(MockMvcRequestBuilders.post("/transaction")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().string(isEmptyString()));

        String expectedResponse = "{\"sum\":1.0,\"avg\":1.0,\"max\":1.0,\"min\":1.0,\"count\":1}";
        mvc.perform(MockMvcRequestBuilders.get("/statistics")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponse));
    }

    // This test may misfire if delay between last POST and next GET
    // the expiry window moves ahead or falls behind on a split second.
    // TODO: tune test to always pass.
    @Test
    @Ignore
    public void getStatisticsHundred() throws Exception {
        int limit = 100;
        Instant testInstant = Instant.now();

        // One transaction a second with 1.0 increment in value for 100 seconds, starting 99 seconds ago.
        // (amount, minus seconds from now)
        // (99, 99), (98, 98), ..., (2, 2), (1, 1), (0, 0)
        List<String> transactions = IntStream.rangeClosed(1, limit)
                .mapToObj((n) ->
                String.format("{\"amount\": %s, \"timestamp\": %s}",
                        limit - n,
                        testInstant.minus(limit - n, ChronoUnit.SECONDS).toEpochMilli()))
                .collect(Collectors.toList());


        for (String transaction : transactions) {
            mvc.perform(MockMvcRequestBuilders.post("/transaction")
                    .content(transaction)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON));
        }

        String expectedResponse = String.format("{\"sum\":%s,\"avg\":%s,\"max\":%s,\"min\":%s,\"count\":%d}",
                /* sum: */ 0.5 * (58 * 59), /* avg: */ 0.5 * (58 * 59) / 59,
                /* max: */ 58.0, /* min: */ 0.0, /* count: */ 59);

        mvc.perform(MockMvcRequestBuilders.get("/statistics")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponse));
    }
}