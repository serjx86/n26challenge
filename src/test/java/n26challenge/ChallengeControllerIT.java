package n26challenge;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.net.URI;
import java.net.URL;
import java.time.Instant;

import n26challenge.model.Statistics;
import n26challenge.model.Transaction;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = {"n26challenge.expireSeconds=60", "n26challenge.attendantPeriodMilliseconds=300"})
public class ChallengeControllerIT {

    @LocalServerPort
    private int port;

    private URL base;
    private URL transaction;
    private URI statistics;

    @Autowired
    private TestRestTemplate template;

    @Before
    public void setUp() throws Exception {
        this.base = new URL("http://localhost:" + port + "/");
        this.transaction = new URL(this.base.toString() + "/transaction");
        this.statistics = new URI(this.base.toString() + "/statistics");
    }

    @Test
    public void testSingleStatistics() throws Exception {

        Transaction transaction = new Transaction(42.0, Instant.now().toEpochMilli());
        template.postForEntity(this.transaction.toString(), transaction, String.class);

        ResponseEntity<Statistics> response = template.getForEntity(this.statistics.toString(), Statistics.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        Statistics statistics = new Statistics(transaction);

        Statistics responseStatistic = response.getBody();
        assertThat(responseStatistic.getSum(), equalTo(statistics.getSum()));

    }
}