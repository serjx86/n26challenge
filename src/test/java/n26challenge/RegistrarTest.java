package n26challenge;

import n26challenge.model.Transaction;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.stream.IntStream;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(properties = {"n26challenge.expireSeconds=60", "n26challenge.attendantPeriodMilliseconds=30000"})
public class RegistrarTest {

    @Autowired
    ApplicationContext applicationContext;


    @Test
    public void testRegistrareInit() {

        Registrar registrar = applicationContext.getAutowireCapableBeanFactory().createBean(Registrar.class); //new Registrar();
        Assert.assertEquals(null, registrar.getCurrentStatistics());

        registrar.update(new Transaction(1.0, Instant.now().toEpochMilli()));

        Assert.assertEquals(1, registrar.getCurrentStatistics().getCount());

        registrar.update(new Transaction(1.0, Instant.now().toEpochMilli()));

        Assert.assertEquals(2, registrar.getCurrentStatistics().getCount());

        registrar.update(new Transaction(1.0, Instant.now().toEpochMilli()));
        registrar.update(new Transaction(1.0, Instant.now().toEpochMilli()));

        Assert.assertEquals(4, registrar.getCurrentStatistics().getCount());
        Assert.assertEquals(4.0, registrar.getCurrentStatistics().getSum(), 0.00001);
        Assert.assertEquals(1.0, registrar.getCurrentStatistics().getAvg(), 0.00001);
        Assert.assertEquals(1.0, registrar.getCurrentStatistics().getMax(), 0.00001);
        Assert.assertEquals(1.0, registrar.getCurrentStatistics().getMin(), 0.00001);

    }

    @Test
    public void testExpiredImperative() {

        Registrar registrar = applicationContext.getAutowireCapableBeanFactory().createBean(Registrar.class);

        registrar.update(new Transaction(1.0, Instant.now().minus(61, ChronoUnit.SECONDS).toEpochMilli()));

        Assert.assertEquals(null, registrar.getCurrentStatistics());

    }

    /**
     * This and many tests rely on the fact that for integers N and M, where M > N:
     * sum(1, 2, ..., N) = (N * (N + 1)) / 2
     * and also
     * sum(N, N + 1, ..., M) = (M * (M + 1) - (N - 1) * N  / 2
     */
    @Test
    public void testExpiredRemoved() {
        RegistrarInstrumentedClock registrar = applicationContext.getAutowireCapableBeanFactory()
                .createBean(RegistrarInstrumentedClock.class);
        int limit = 100;
        Instant testInstant = Instant.now();

        // One transaction a second with 1.0 increment in value for 100 seconds, starting 99 seconds ago.
        IntStream.rangeClosed(1, limit).forEach((n) ->
                registrar.update(
                        new Transaction(n, testInstant.minus(limit - n, ChronoUnit.SECONDS).toEpochMilli())));
        // (amount, minus seconds from now)
        // (1, 99), (2, 98), ..., (98, 2), (99, 1), (100, 0)

        // Only 60 newest transactions should be kept out of 100.
        Assert.assertEquals(60, registrar.getCurrentStatistics().getCount());
        Assert.assertEquals(0.5 * (100 * 101 - 40 * 41), registrar.getCurrentStatistics().getSum(), 0.001);
        Assert.assertEquals(0.5 * (100 * 101 - 40 * 41) / 60, registrar.getCurrentStatistics().getAvg(), 0.001);
        Assert.assertEquals(100, registrar.getCurrentStatistics().getMax(), 0.001);
        Assert.assertEquals(41, registrar.getCurrentStatistics().getMin(), 0.001);

        // Let's move the clock ahead 30 seconds.
        registrar.setClockAt(testInstant.plus(30, ChronoUnit.SECONDS));

        registrar.update();
        // Update should leave 30 most recent transactions, amounts in range [100, 71].
        Assert.assertEquals(30, registrar.getCurrentStatistics().getCount());
        Assert.assertEquals(0.5 * (100 * 101 - 70 * 71), registrar.getCurrentStatistics().getSum(), 0.001);
        Assert.assertEquals(100, registrar.getCurrentStatistics().getMax(), 0.001);
        Assert.assertEquals(71, registrar.getCurrentStatistics().getMin(), 0.001);

        // Clock moves ahead 58 seconds, shrinking window to 2 transactions.
        registrar.setClockAt(testInstant.plus(58, ChronoUnit.SECONDS));

        registrar.update();

        Assert.assertEquals(2, registrar.getCurrentStatistics().getCount());
        Assert.assertEquals(100 + 99, registrar.getCurrentStatistics().getSum(), 0.001);
        Assert.assertEquals(100, registrar.getCurrentStatistics().getMax(), 0.001);
        Assert.assertEquals(99, registrar.getCurrentStatistics().getMin(), 0.001);

        // Clock is 61 seconds past newest transaction. No transactions should be visible.
        registrar.setClockAt(testInstant.plus(61, ChronoUnit.SECONDS));

        registrar.update();

        Assert.assertEquals(null, registrar.getCurrentStatistics());

    }

    @Test
    public void testExpiredRemoved2() {
        RegistrarInstrumentedClock registrar = applicationContext.getAutowireCapableBeanFactory()
                .createBean(RegistrarInstrumentedClock.class);
        int limit = 100;
        Instant testInstant = Instant.now();

        // One transaction a second with 1.0 increment in value for 100 seconds, starting 99 seconds ago.
        IntStream.rangeClosed(1, limit).forEach((n) ->
                registrar.update(
                        new Transaction(limit - n, testInstant.minus(limit - n, ChronoUnit.SECONDS).toEpochMilli())));
        // (amount, minus seconds from now)
        // (99, 99), (98, 98), ..., (2, 2), (1, 1), (0, 0)

        // Only 60 newest transactions should be kept out of 100.
        Assert.assertEquals(60, registrar.getCurrentStatistics().getCount());
        Assert.assertEquals(0.5 * (59 * 60), registrar.getCurrentStatistics().getSum(), 0.001);
        Assert.assertEquals(0.5 * (59 * 60) / 60, registrar.getCurrentStatistics().getAvg(), 0.001);
        Assert.assertEquals(59, registrar.getCurrentStatistics().getMax(), 0.001);
        Assert.assertEquals(0, registrar.getCurrentStatistics().getMin(), 0.001);
    }
}
