package n26challenge;

import org.springframework.stereotype.Component;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;

/**
 * This class lets us go Slow Möbius on our clock without sticking fingers stuck in the clock gears.
 * Wars were fought for less.
 */
@Component
public class RegistrarInstrumentedClock extends Registrar {

    private Clock clock = Clock.systemDefaultZone();

    public RegistrarInstrumentedClock() {
    }

    public void setClockAt(Instant instant) {
        this.clock = Clock.fixed(instant, ZoneId.systemDefault());
    }

    @Override
    protected Instant getExpiry() {
        return Instant.now(this.clock).minus(Duration.of(this.expirySeconds, ChronoUnit.SECONDS));
    }

}
