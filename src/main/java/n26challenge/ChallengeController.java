package n26challenge;

import n26challenge.model.Statistics;
import n26challenge.model.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;


@RestController
public class ChallengeController {

    private static final Logger logger = LoggerFactory.getLogger(ChallengeController.class);

    @Autowired
    Registrar registrar;

    @Autowired
    Attendant attendant;

    @PostConstruct
    void startAttendant() {
        attendant.start();
    }

    @PostMapping("/transaction")
    public ResponseEntity postTransaction(@RequestBody Transaction transaction) {
        logger.debug(transaction.toString());
        if (registrar.isExpired(transaction)) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        registrar.update(transaction);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping("/statistics")
    public ResponseEntity<Statistics> getStatistics() {
        Statistics statistics = this.registrar.getCurrentStatistics();
        logger.debug(statistics != null ? statistics.toString() : "No statistics to report.");
        return new ResponseEntity<>(statistics, HttpStatus.OK);
    }

}
