package n26challenge.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import static java.lang.Double.max;
import static java.lang.Double.min;

public class Statistics implements Transient {

    private double sum;
    private double avg;
    private double max;
    private double min;
    private long count;
    @JsonIgnore
    private long timestamp;

    public Statistics() {
    }

    public Statistics(Transaction transaction) {
        double amount = transaction.getAmount();
        this.sum = this.avg = this.max = this.min = amount;
        this.count = 1;
        this.timestamp = transaction.getTimestamp();
    }

    public void update(Transaction transaction) {
        double amount = transaction.getAmount();
        this.count++;
        this.sum += amount;
        this.avg = this.sum / this.count;
        this.max = max(this.max, amount);
        this.min = min(this.min, amount);
    }

    public double getAvg() {
        return avg;
    }

    public double getMax() {
        return max;
    }

    public double getMin() {
        return min;
    }

    public long getCount() {
        return count;
    }

    public double getSum() {
        return sum;
    }

    public long getTimestamp() {
        return timestamp;
    }

}
