package n26challenge.model;

public interface Transient {
    long getTimestamp();
}
