package n26challenge;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Attendant component schedules regular cleanup for any expired transactions in in Registrar component.
 */
@Component
public class Attendant {

    @Autowired
    Registrar registrar;

    @Value("${n26challenge.attendantPeriodMilliseconds}")
    protected int period;

    Timer timer;

    public Attendant() {
    }

    private class Task extends TimerTask {
        @Override
        public void run() {
            registrar.update();
        }
    }

    public void start() {
        this.timer = new Timer(Attendant.class.getName());
        this.timer.scheduleAtFixedRate(new Task(), this.period, this.period);
    }

    public void stop() {
        this.timer.cancel();
    }

}
