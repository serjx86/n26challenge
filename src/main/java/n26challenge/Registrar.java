package n26challenge;

import n26challenge.model.Statistics;
import n26challenge.model.Transaction;
import n26challenge.model.Transient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.concurrent.locks.ReentrantLock;


/**
 * Registrar class stores all valid transactions and aggregates their statistics, like count, sum, max, min, average.
 * <p>
 * Implementation details: ArrayList is used as unbounded queue. ConcurrentLinkedDeque could be used and would require
 * less explicit synchronization, however, ArrayList allows removing range in one fell swoop, and this may offer better
 * performance if transactions come in bursts and expire around the same time. Merit of either queue implementation
 * could be judged only against live data and changes should be made accordingly.
 * <p>
 * Attendant component is tasked with scheduling periodic cleanup for expired transactions.
 */
@Component
public class Registrar {

    @Value("${n26challenge.expireSeconds}")
    protected int expirySeconds;

    private Statistics currentStatistics;

    private ArrayList<Statistics> statisticsQueue = new ArrayList<>();

    final ReentrantLock statisticQueueLock = new ReentrantLock();

    private final ReentrantLock latestStatisticsLock = new ReentrantLock();

    protected Instant getExpiry() {
        return Instant.now().minus(Duration.of(this.expirySeconds, ChronoUnit.SECONDS));
    }

    public boolean isExpired(Transient transaction) {
        return this.getExpiry().toEpochMilli() > transaction.getTimestamp();
    }

    /**
     * Transaction will be added to the queue tail of the queue.
     * Each element in the queue will be updated until one of the conditions is met:
     * queue is exhausted, element with expired timestamp is found. In the latter case
     * statistics queue will be trimmed from the expired element to the head.
     * <p>
     * Should run in linear time.
     *
     * @param transaction a Transaction instance.
     */
    public void update(Transaction transaction) {
        if (transaction == null) {
            this.update();
            return;
        }

        statisticQueueLock.lock();
        Statistics previousStatistics = null;
        try {
            this.statisticsQueue.add(0, new Statistics(transaction));
            int pastExpiryIndex = -1;
            int index = 0;

            for (Statistics statistics : this.statisticsQueue) {
                if (previousStatistics != null) {
                    statistics.update(transaction);
                }
                index++;
                if (this.isExpired(statistics)) {
                    pastExpiryIndex = index;
                    break;
                }
                previousStatistics = statistics;
            }
            if (pastExpiryIndex > -1) {
                this.statisticsQueue.subList(pastExpiryIndex, this.statisticsQueue.size()).clear();
            }
        } finally {
            statisticQueueLock.unlock();
        }

        this.setCurrentStatistics(previousStatistics);
    }

    /**
     * Walk the statistics queue backwards and remove each element that is expired.
     * <p>
     * The queue is walked backwards, to walk expired elements only and stop iteration
     * once walid element is encountered. However, it is possible to use binary search to identify
     * earliest expired element and remove all expired elements after it as in update with Transaction overload.
     * <p>
     * Should run in linear time.
     */
    public void update() {
        if (!statisticQueueLock.tryLock()) {
            return;
        }
        try {
            long expiry = this.getExpiry().toEpochMilli();
            ListIterator<Statistics> iterator = this.statisticsQueue.listIterator(this.statisticsQueue.size());

            while (iterator.hasPrevious()) {
                Statistics statistics = iterator.previous();
                if (statistics.getTimestamp() <= expiry) {
                    iterator.remove();
                } else {
                    break;
                }
            }
        } finally {
            this.statisticQueueLock.unlock();
        }

        if (this.statisticsQueue.size() > 0) {
            this.setCurrentStatistics(this.statisticsQueue.get(this.statisticsQueue.size() - 1));
        } else {
            this.setCurrentStatistics(null);
        }
    }

    private void setCurrentStatistics(Statistics statistics) {
        this.latestStatisticsLock.lock();
        try {
            this.currentStatistics = statistics;
        } finally {
            this.latestStatisticsLock.unlock();
        }
    }

    /**
     * Returns current statistic. Methid is synchronized and may block briefly during update.
     *
     * @return Current statistics.
     */
    public Statistics getCurrentStatistics() {
        this.latestStatisticsLock.lock();
        try {
            return this.currentStatistics;
        } finally {
            this.latestStatisticsLock.unlock();
        }
    }

}
